class ApplicationController < ActionController::Base
  require 'net/http'

  protected

  def send_request(request_url, request_body = {})
    url = "https://rth-recruitment.herokuapp.com/api/candidates/register"
    uri = URI.parse(url)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = http_method.new("#{uri.path}?#{uri.query}", request_headers)
    request.body = request_body.to_query
    response = http.request(request)

    response.body
  end

  private

  def http_method
    Net::HTTP::Post
  end

  def request_headers
    {
      "Content-Type" => "application/x-www-form-urlencoded",
      "X-App-Token" => "76524a53ee60602ac3528f38"
    }
  end
end
