class RegistersController < ApplicationController
  def show
    description = "Hello, My name is Thinh. As a Ruby Developer, I have experience in web-development at both front-end and back-end along with foreigner customers from Japan and Hong Kong. I'm able to write clean, readable and maintainable Ruby code and also can work independently or in a team as well."
    request_body = {
      email: 'thinh.cao@hotmail.com',
      first_name: 'Thinh',
      last_name: 'Cao',
      candidate_type: 'ror',
      description: description
    }
    request_url = "https://rth-recruitment.herokuapp.com/api/candidates/register"

    render json: { results: send_request(request_url, request_body) }
  end
end
